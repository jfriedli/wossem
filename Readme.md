# 🔥 Wossem 🔥

Wodpress plugins Semgrep scanner automation

# What for? (｡•́‿  •̀｡)
This script scans every available Wordpress plugin listed on the official website using [Semgrep](https://semgrep.dev/).
All the findings are stored to disk and notifications sent to a Telegram chat.
The findings can be reviewed and looted 🤑💰

# Configuration ⚒️

Some evnvironment variables need additional configuration. 
Note that a `.env` file will be loaded (you need to create it, if you want to use it).

### Mandatory 📜

`TELEGRAM_BOT_TOKEN=[redacted]`: The Telegram bot [token](https://core.telegram.org/bots/tutorial#obtain-your-bot-token).

`TELEGRAM_CHAT_ID=[redacted]`: The chat id of the chat the bot will send the findings to. You can use the `Telegram Bot Raw` to find your chat id.

**Note:** Make sure you already interacted with your bot, to be able to receive messsages!

 ### Optional 🤙

 `LOG_LEVEL_FILTER`: Set the log level you want to receive (currently only one accepted). [Possible](https://semgrep.dev/docs/writing-rules/rule-syntax/) values are `INFO`, `WARNING` or `ERROR`. The default value is *ERROR*.


 The JSON file `excludes.json` allows you to exclude `check_id`s from your finding notifications.

 # How To Run

 Install dependencies `pip install -r requirements.txt`

 Run the script `python wossem.py`

 Enjoy 🎉🎉