import requests
import os
import json
import shutil
import subprocess
from requests.exceptions import HTTPError
from zipfile import ZipFile

import telebot
from loguru import logger
from dotenv import load_dotenv

load_dotenv()

# Wordpress plugin api, the page max is 250
PLUGIN_API = "https://api.wordpress.org/plugins/info/1.2/?action=query_plugins&request[page]=PAGE_PLACEHOLDER&request[per_page]=250"
PLUGIN_CACHE_FILE = "plugin_list.json"
telegram_bot = telebot.TeleBot(os.getenv("TELEGRAM_BOT_TOKEN"))
global_exclude_rules = dict()

with open("exclusions.json") as excludes_file:
    global_exclude_rules = json.loads(excludes_file.read())


# Fetch defined plugins page and return paging info and plugin list
def fetch_plugin_page(page: str):
    try:
        response = requests.get(PLUGIN_API.replace("PAGE_PLACEHOLDER", page))
        response.raise_for_status()
        jsonResponse = response.json()

        return jsonResponse["info"], jsonResponse["plugins"]

    except HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
        return None, None
    except Exception as err0:
        print(f"Other error occurred: {err0}")
        return None, None


# downloads a plguins into a tmp folder and returns its path
def fetch_plugin(slug: str, url: str, version: str, tmp_folder: str):
    r = requests.get(url, allow_redirects=True)
    file_path = tmp_folder + "/" + slug + "-" + version
    file_name = file_path + ".zip"
    open(file_name, "wb").write(r.content)

    with ZipFile(file_name, "r") as zObject:
        zObject.extractall(path=file_path)
        logger.debug(file_path)
    return file_path


# run semgrep and store results in json file and return the filepath
def run_semgrep(folder: str, slug: str, version: str):
    logger.info("Scanning " + folder)
    outfile = os.getcwd() + "/output/scan_results_" + slug + "_" + version + ".json"
    subprocess.run(
        "semgrep scan -q --config p/phpcs-security-audit --config p/command-injection --config p/default --json --output "
        + outfile
        + " "
        + folder,
        shell=True,
    )
    logger.info("Scan finished " + outfile)
    return outfile


# just get rid of unnecessary files
def cleanup_plugin_folder(folder: str):
    shutil.rmtree(folder, ignore_errors=True)


# load plugins from cache
def load_plugin_list_cache():
    try:
        with open(PLUGIN_CACHE_FILE, "r") as openfile:
            return json.load(openfile)
    except:
        return []


# load a result file or empty dict
def load_semgrep_result_file(path: str):
    try:
        with open(path, "r") as openfile:
            return json.load(openfile)
    except:
        return {}


# store plugins to file cache
def write_plugin_list_cache(plugins):
    jsons = json.dumps(plugins, indent=4)
    with open(PLUGIN_CACHE_FILE, "w") as file:
        file.write(jsons)


# parse json results from semgrep and make it good looking markdown
def semgrep_result_to_markup(results: dict):
    has_findings = False
    markdown = ""

    for result in results["results"]:
        if result["extra"]["severity"] == os.getenv(
            "LOG_LEVEL_FILTER", "ERROR"
        ) and not test_check_id_exclude(result["check_id"]):
            has_findings = True
            try:
                markdown += "\n  Severity:" + result["extra"]["severity"]
                markdown += "\n  Rule:" + result["check_id"]
                markdown += "\n  Path:" + result["path"]
                markdown += "\n  Lines: `" + result["extra"]["lines"] + "` \n\n"
            except:
                markdown += " Sorry failed to parse"

    if has_findings:
        return "\n \n \n  # Results \n\n" + markdown
    else:
        return None


# Test if check_id has been excluded by config file
def test_check_id_exclude(check_id: str):
    return check_id in global_exclude_rules["check_id"]


# parse the semgrep results and send telgeram notifications via bot
def send_notifications(result_file: str):
    results = load_semgrep_result_file(result_file)

    markup = semgrep_result_to_markup(results)
    if markup:
        send_telegram_message(markup)
    else:
        logger.info("No ERROR results for " + result_file)


# Send a Telegram message
# Split it into multiple message if too large
def send_telegram_message(msg: str):
    try:
        logger.info("Notify Telegram")
        splitted_text = telebot.util.split_string(msg, 3000)
        for text in splitted_text:
            telegram_bot.send_message(
                os.getenv("TELEGRAM_CHAT_ID"), text, parse_mode="MARKDOWN"
            )
    except Exception as error:
        logger.warning("Notify Telegram failed" + str(error))


# scan a plugin
def scan_plugin(plugin: dict, tmp_dir: str):
    try:
        logger.info("Fetching " + plugin["download_link"])
        plugin_folder = fetch_plugin(
            plugin["slug"], plugin["download_link"], plugin["version"], tmp_dir
        )
        result_file = run_semgrep(plugin_folder, plugin["slug"], plugin["version"])
        send_notifications(result_file)
        cleanup_plugin_folder(plugin_folder)
    except Exception as e:
        error_string = (
            "Failed scanning a plugin" + str(e) + " for plugin " + str(plugin)
        )
        logger.error(error_string)
        send_telegram_message(error_string)


# download all plugin metadata and cache it
def fetch_all_plugins_metadata():
    paging, _ = fetch_plugin_page("1")
    cached_plugins = load_plugin_list_cache()
    plugins = []

    # @todo use hash so plugin versions are as well tested
    logger.info(
        "cached plugins: "
        + str(len(cached_plugins))
        + " remote plugins: "
        + str(paging["results"])
    )
    if len(cached_plugins) == paging["results"]:
        logger.info("Using cached plugins list")
        plugins = cached_plugins
    else:
        logger.info("Downloading plugins list")
        for page in range(paging["page"], paging["pages"] + 1):
            _, plugins_page = fetch_plugin_page(str(page))
            plugins = plugins + plugins_page
            logger.debug(
                "downloaded plugins page: " + str(page) + " total: " + str(len(plugins))
            )
        logger.info("fetched nr of plugins: " + str(len(plugins)))

    write_plugin_list_cache(plugins)
    return plugins


# print banner <3
def print_banner():
    with open("banner.txt") as banner:
        logger.info(banner.read())
