import glob
import json
import os
import shutil

from jinja2 import Environment, FileSystemLoader
from loguru import logger
from tqdm import tqdm


# Generate a single HTML file with all findings
def generate_html_report(out_file):
    file_loader = FileSystemLoader("templates")
    env = Environment(loader=file_loader, autoescape=True)
    plugin_template = env.get_template("plugin.html")
    write_file_header(out_file)

    for result_filename in tqdm(glob.glob("./output/*.json"), "Rendering .."):
        handle_single_result(result_filename, out_file, plugin_template)

    write_file_bottom(out_file)
    logger.success("Generated report: " + out_file)
    logger.success("Opening Now")
    os.startfile(out_file)


# render a single result JSON
def handle_single_result(plugin_result_filename: str, out_file: str, template):
    with open(plugin_result_filename) as f:
        try:
            results = json.loads(f.read())
            output = template.render(plugin=results)
            append_plugin_template(out_file, output)
        except Exception as e:
            logger.error(str(e))


# Append the rendered plugin template for memory reasons
def append_plugin_template(out_file, output):
    with open(out_file, "a", encoding="utf-8") as outfile:
        outfile.write(output)


# Init HTML file with header
def write_file_header(filename: str):
    shutil.copyfile("./templates/index_header.html", filename)


# Append missing HTML template to complete file
def write_file_bottom(filename: str):
    with open(filename, "a", encoding="utf-8") as outfile:
        with open(
            "./templates/index_bottom.html", "r", encoding="utf-8"
        ) as bottom_file:
            outfile.write(bottom_file.read())
