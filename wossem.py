import argparse
import tempfile
import multiprocessing
import functools
from datetime import datetime

import helpers.scan as s
import helpers.html as h

from dotenv import load_dotenv

load_dotenv()

parser = argparse.ArgumentParser(
    description="Scan official Wordpress plugins with Semgrep 🌚 🏴"
)
parser.add_argument(
    "-scan", "--scan", action="store_true", help="Scan all plugins", required=False
)
parser.add_argument(
    "-html",
    "--html",
    action="store_true",
    help="Convert all findings into an HTML file",
    required=False,
)
args = parser.parse_args()

s.print_banner()

if args.scan:
    s.send_telegram_message("🌚 Wossem 🌚 Bot was started")
    temp_dir = tempfile.TemporaryDirectory()
    all_plugins = s.fetch_all_plugins_metadata()

    with multiprocessing.Pool() as pool:
        pool.map(functools.partial(s.scan_plugin, tmp_dir=temp_dir.name), all_plugins)

    pool.close()
    temp_dir.cleanup()
    s.send_telegram_message(" 🌚 🏴 🌚 🏴 Wossem Bot has finished 🙆🙆")

if args.html:
    out_file = "wossem_results_" + datetime.now().strftime("%Y_%m_%d_%H_%M") + ".html"
    h.generate_html_report(out_file)
